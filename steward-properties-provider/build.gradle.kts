@file:Suppress("UNCHECKED_CAST")

dependencies {
	//
	implementation(libraries["org.springframework.boot:spring-boot-starter-json"])
	//
	implementation(project(":steward-misc4j"))
	//
	//
	testCompileOnly(libraries["com.google.auto.service:auto-service"])
	testAnnotationProcessor(libraries["com.google.auto.service:auto-service"])
	//
	testImplementation(libraries["org.springframework.boot:spring-boot-starter"])
}
//
(extra["configureMavenPublication"] as () -> Unit).invoke()
