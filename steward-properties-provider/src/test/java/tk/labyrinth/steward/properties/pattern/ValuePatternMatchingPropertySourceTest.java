package tk.labyrinth.steward.properties.pattern;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest
@TestPropertySource(properties = "someVersion = 1.2.three.4b")
class ValuePatternMatchingPropertySourceTest {

	@Value("${someVersion.major}")
	private Integer someVersionMajor;

	@Value("${someVersion.patch}")
	private String someVersionPatch;

	@Test
	void test() {
		Assertions.assertEquals(1, someVersionMajor);
		Assertions.assertEquals("three", someVersionPatch);
	}

	@SpringBootApplication
	static class TestConfiguration {
		// empty
	}
}