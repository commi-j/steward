package tk.labyrinth.steward.properties.provider.test;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "no.op")
public class TestApplication {

	@Value("${spring.steward.foo}")
	private String foo;

	public static void main(String... args) {
		SpringApplication.run(TestApplication.class);
	}
}
