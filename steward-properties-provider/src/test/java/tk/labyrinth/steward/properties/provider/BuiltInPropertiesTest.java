package tk.labyrinth.steward.properties.provider;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.context.ConfigurableApplicationContext;
import tk.labyrinth.steward.properties.provider.test.BuiltInPropertiesTestBase;

import java.util.List;
import java.util.UUID;

class BuiltInPropertiesTest extends BuiltInPropertiesTestBase {

	/**
	 * Collides with system property PID, must have lesser priority.
	 */
	@Test
	void testPid() {
		ConfigurableApplicationContext applicationContext = runApplication();
		//
		Assertions.assertEquals(List.of(-1), getBuiltInProperties(applicationContext, "PID", Integer.class));
		Assertions.assertNotEquals(-1, applicationContext.getEnvironment().getProperty("PID", Integer.class));
	}

	/**
	 * Value must be different for each run.
	 */
	@Test
	void testRandomUuid() {
		UUID firstRun = runApplication().getEnvironment().getProperty("random.uuid", UUID.class);
		UUID secondRun = runApplication().getEnvironment().getProperty("random.uuid", UUID.class);
		//
		Assertions.assertNotNull(firstRun);
		Assertions.assertNotNull(secondRun);
		Assertions.assertNotEquals(firstRun, secondRun);
	}

	/**
	 * Checking if property is present.
	 */
	@Test
	void testSimple() {
		Assertions.assertEquals("SIMPLE_PROPERTY",
				runApplication().getEnvironment().getProperty("simple.property"));
	}
}
