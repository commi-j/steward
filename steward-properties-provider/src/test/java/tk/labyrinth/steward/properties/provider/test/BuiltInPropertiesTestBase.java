package tk.labyrinth.steward.properties.provider.test;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.AbstractEnvironment;
import tk.labyrinth.steward.properties.provider.BuiltInMapPropertySource;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class BuiltInPropertiesTestBase {

	@BeforeEach
	private void beforeEach() {
		System.clearProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME);
	}

	public <T> List<T> getBuiltInProperties(ConfigurableApplicationContext applicationContext, String key, Class<T> type) {
		return applicationContext.getEnvironment().getPropertySources().stream()
				.filter(BuiltInMapPropertySource.class::isInstance)
				.map(propertySource -> propertySource.getProperty("PID"))
				.filter(Objects::nonNull)
				.map(type::cast)
				.collect(Collectors.toList());
	}

	public ConfigurableApplicationContext runApplication(String... activeProfiles) {
		System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, String.join(",", activeProfiles));
		return SpringApplication.run(TestApplication.class);
	}
}
