package tk.labyrinth.steward.properties.provider.test;

import com.google.auto.service.AutoService;
import org.springframework.context.annotation.Profile;
import tk.labyrinth.steward.properties.provider.BuiltInProperties;
import tk.labyrinth.steward.properties.provider.PropertyKey;

import java.util.UUID;

public class TestBuiltInProperties implements BuiltInProperties {

	@AutoService(BuiltInProperties.class)
	public static class CommonProperties implements BuiltInProperties {

		@PropertyKey("PID")
		private final Integer pid = -1;

		@PropertyKey("random.uuid")
		private final UUID randomUuid = UUID.randomUUID();

		@PropertyKey("simple.property")
		private final String simpleProperty = "SIMPLE_PROPERTY";

		@PropertyKey("spring.steward.foo")
		private final String springStewardFoo = "BAR";

		@Profile("blue")
		@PropertyKey("colour.property")
		private static String colourPropertyInBlueMethod() {
			return "BLUE";
		}
	}

	@AutoService(BuiltInProperties.class)
	@Profile("red")
	public static class RedProperties implements BuiltInProperties {

		@PropertyKey("colour.property")
		private final String colourPropertyInRedType = "RED";
	}
}
