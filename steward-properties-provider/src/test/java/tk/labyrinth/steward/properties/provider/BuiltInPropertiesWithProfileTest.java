package tk.labyrinth.steward.properties.provider;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.core.env.AbstractEnvironment;
import tk.labyrinth.steward.properties.provider.test.BuiltInPropertiesTestBase;

public class BuiltInPropertiesWithProfileTest extends BuiltInPropertiesTestBase {

	@Test
	void testNoProfile() {
		Assertions.assertNull(System.getProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME));
		Assertions.assertNull(runApplication().getEnvironment().getProperty("colour.property"));
	}

	@Test
	void testProfileOnMethod() {
		Assertions.assertNull(System.getProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME));
		Assertions.assertEquals("BLUE",
				runApplication("blue").getEnvironment().getProperty("colour.property"));
	}

	@Test
	void testProfileOnType() {
		Assertions.assertNull(System.getProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME));
		Assertions.assertEquals("RED",
				runApplication("red").getEnvironment().getProperty("colour.property"));
	}
}
