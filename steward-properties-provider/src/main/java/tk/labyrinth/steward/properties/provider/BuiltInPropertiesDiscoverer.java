package tk.labyrinth.steward.properties.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import tk.labyrinth.steward.misc4j.java.util.ServiceLoaderUtils;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BuiltInPropertiesDiscoverer implements EnvironmentPostProcessor {

	@Override
	public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
		Set<BuiltInProperties> builtInPropertiesSet = ServiceLoaderUtils.load(BuiltInProperties.class);
		//
		builtInPropertiesSet.forEach(builtInProperties -> {
			List<Field> relevantFields = Stream.of(builtInProperties.getClass().getDeclaredFields())
					.filter(declaredField -> declaredField.getAnnotation(PropertyKey.class) != null)
					.filter(declaredField -> acceptsProfiles(environment, declaredField))
					.collect(Collectors.toList());
			List<Method> relevantMethods = Stream.of(builtInProperties.getClass().getDeclaredMethods())
					.filter(declaredMethod -> declaredMethod.getAnnotation(PropertyKey.class) != null)
					.filter(declaredMethod -> acceptsProfiles(environment, declaredMethod))
					.collect(Collectors.toList());
			//
			Map<String, Object> properties = new HashMap<>();
			//
			relevantFields.forEach(field -> {
				try {
					field.setAccessible(true);
					properties.put(
							field.getAnnotation(PropertyKey.class).value(),
							field.get(builtInProperties));
				} catch (IllegalAccessException ex) {
					throw new RuntimeException(ex);
				}
			});
			relevantMethods.forEach(method -> {
				try {
					method.setAccessible(true);
					properties.put(
							method.getAnnotation(PropertyKey.class).value(),
							method.invoke(builtInProperties));
				} catch (ReflectiveOperationException ex) {
					throw new RuntimeException(ex);
				}
			});
			//
			environment.getPropertySources().addLast(new BuiltInMapPropertySource(builtInProperties.getClass(), properties));
		});
	}

	private static <M extends AccessibleObject & Member> boolean acceptsProfiles(Environment environment, M member) {
		boolean result;
		{
			// TODO: Replace with metadata scanning, like in org.springframework.context.annotation.ProfileCondition#matches.
			//
			Profile memberProfileAnnotation = member.getAnnotation(Profile.class);
			Profile typeProfileAnnotation = member.getDeclaringClass().getAnnotation(Profile.class);
			String[] profileValues = Stream.of(memberProfileAnnotation, typeProfileAnnotation)
					.filter(Objects::nonNull)
					.map(Profile::value)
					.flatMap(Stream::of)
					.toArray(String[]::new);
			if (profileValues.length > 0) {
				result = environment.acceptsProfiles(Profiles.of(profileValues));
			} else {
				// TODO: Check behaviour is @Profile with empty array is provided.
				//
				result = true;
			}
		}
		return result;
	}
}
