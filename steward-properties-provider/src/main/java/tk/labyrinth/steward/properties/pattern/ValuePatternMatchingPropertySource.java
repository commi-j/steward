package tk.labyrinth.steward.properties.pattern;

import org.springframework.core.env.PropertySource;
import org.springframework.core.env.PropertySources;

import javax.annotation.Nullable;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * https://semver.org/
 *
 * @author Commitman
 * @version 1.0.0
 */
public class ValuePatternMatchingPropertySource extends PropertySource<PropertySources> {

	private final Map<String, Pattern> propertyValuePatterns;

	public ValuePatternMatchingPropertySource(PropertySources propertySources, Map<String, Pattern> propertyValuePatterns) {
		super(ValuePatternMatchingPropertySource.class.getName(), propertySources);
		this.propertyValuePatterns = propertyValuePatterns;
	}

	@Nullable
	@Override
	public Object getProperty(String name) {
		// Example:
		//  1. Receiving 'hello.world';
		//  2. Checking if we have registered pattern for 'hello';
		//  3. Looking for 'hello' in other PropertySources;
		//  4. Resolving property against pattern and returning value for 'world' group.
		//
		Object result;
		{
			int lastIndexOfDot = name.lastIndexOf('.');
			if (lastIndexOfDot != -1) {
				String basePropertyName = name.substring(0, lastIndexOfDot);
				Pattern propertyValuePattern = propertyValuePatterns.get(basePropertyName);
				if (propertyValuePattern != null) {
					Object basePropertyValue = findInPropertySources(getSource(), basePropertyName);
					if (basePropertyValue != null) {
						Matcher matcher = propertyValuePattern.matcher(basePropertyValue.toString());
						if (matcher.find()) {
							result = matcher.group(name.substring(lastIndexOfDot + 1));
						} else {
							result = null;
						}
					} else {
						result = null;
					}
				} else {
					result = null;
				}
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	public static Object findInPropertySources(PropertySources propertySources, String propertyName) {
		return propertySources.stream()
				// TODO: Filter out self.
				.map(propertySource -> propertySource.getProperty(propertyName))
				.filter(Objects::nonNull)
				.findFirst().orElse(null);
	}
}
