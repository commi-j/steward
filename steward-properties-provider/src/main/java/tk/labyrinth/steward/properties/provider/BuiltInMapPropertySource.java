package tk.labyrinth.steward.properties.provider;

import org.springframework.core.env.MapPropertySource;

import java.util.Map;

public class BuiltInMapPropertySource extends MapPropertySource {

	public BuiltInMapPropertySource(Class<?> cl, Map<String, Object> source) {
		super(cl.getCanonicalName(), source);
	}
}
