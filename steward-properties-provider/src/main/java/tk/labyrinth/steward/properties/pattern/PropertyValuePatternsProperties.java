package tk.labyrinth.steward.properties.pattern;

import lombok.Value;

import java.util.Map;

/**
 * @author Commitman
 * @version 1.0.0
 */
@Value
public class PropertyValuePatternsProperties {

	Map<String, String> patterns;

	Map<String, String> properties;
}
