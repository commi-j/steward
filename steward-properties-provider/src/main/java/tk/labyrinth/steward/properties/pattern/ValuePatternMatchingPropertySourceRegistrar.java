package tk.labyrinth.steward.properties.pattern;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.apache.commons.logging.Log;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author Commitman
 * @version 1.0.1
 */
@RequiredArgsConstructor
public class ValuePatternMatchingPropertySourceRegistrar implements EnvironmentPostProcessor {

	public static final String propertyValuePatternsFileName = "propertyValuePatterns.json";

	private final Log logger;

	@Override
	public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
		Map<String, Pattern> propertyValuePatterns;
		{
			Resource resource = new DefaultResourceLoader().getResource(propertyValuePatternsFileName);
			if (resource.exists()) {
				PropertyValuePatternsProperties properties;
				try (InputStream inputStream = resource.getInputStream()) {
					//
					// We use new ObjectMapper because there is no common one at this moment.
					// We also do not want any interference with it.
					properties = new ObjectMapper().readValue(inputStream, PropertyValuePatternsProperties.class);
				} catch (IOException ex) {
					throw new RuntimeException(ex);
				}
				Map<String, Pattern> resolvedPatterns = properties.getPatterns().entrySet().stream()
						.collect(Collectors.toMap(
								Map.Entry::getKey,
								entry -> resolvePropertyValuePattern(entry.getValue(), Map.of())));
				propertyValuePatterns = properties.getProperties().entrySet().stream()
						.collect(Collectors.toMap(
								Map.Entry::getKey,
								entry -> resolvePropertyValuePattern(entry.getValue(), resolvedPatterns)));
			} else {
				propertyValuePatterns = null;
			}
		}
		//
		if (propertyValuePatterns != null) {
			ValuePatternMatchingPropertySource propertySource = new ValuePatternMatchingPropertySource(
					environment.getPropertySources(),
					propertyValuePatterns);
			environment.getPropertySources().addLast(propertySource);
			logger.info("ValuePatternMatchingPropertySource registered with " + propertyValuePatterns.size() + " patterns");
		} else {
			logger.info(propertyValuePatternsFileName + " not found -> ValuePatternMatchingPropertySource will not be registered");
		}
	}

	private static Pattern resolvePropertyValuePattern(
			String prefixedPattern,
			Map<String, Pattern> resolvedPatterns) {
		Pattern result;
		{
			String[] splitPattern = prefixedPattern.split(":");
			switch (splitPattern[0]) {
				case "pattern":
					result = resolvedPatterns.get(splitPattern[1]);
					break;
				case "regexp":
					result = Pattern.compile(splitPattern[1]);
					break;
				default:
					throw new IllegalArgumentException("Unknown prefix: " + prefixedPattern);
			}
		}
		return result;
	}
}
