# Directions

- Inbound
    - @RestController
    - @KafkaListener
- Outbound
    - @FeignClient
- Persistence
    - @Entity
    - @Document