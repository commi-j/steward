@file:Suppress("UNCHECKED_CAST")

dependencies {
	//
	implementation(libraries["tk.labyrinth:jaap"])
	//
	implementation(project(":steward-manifest-model"))
	implementation(project(":steward-misc4j"))
	//
	//
	compileOnly(libraries["org.springframework.boot:spring-boot-starter-web"])
	//
	//
	testImplementation(libraries["javax.json.bind:javax.json.bind-api"])
	testImplementation(libraries["org.springframework.boot:spring-boot-starter-data-jpa"])
	testImplementation(libraries["org.springframework.boot:spring-boot-starter-validation"])
	testImplementation(libraries["org.springframework.boot:spring-boot-starter-web"])
	//
	testAnnotationProcessor(project(":steward-manifest-builder"))
}
//
(extra["configureMavenPublication"] as () -> Unit).invoke()
