package tk.labyrinth.steward.manifestbuilder.discovery;

import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;

import java.util.stream.Stream;

public interface ElementDiscoverer {

	Stream<? extends ElementTemplate> discover(TypeElementTemplate typeElementHandle);
}
