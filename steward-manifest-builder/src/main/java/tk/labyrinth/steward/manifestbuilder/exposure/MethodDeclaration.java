package tk.labyrinth.steward.manifestbuilder.exposure;

import lombok.Builder;
import lombok.Value;
import tk.labyrinth.jaap.model.declaration.FormalParameterDeclaration;
import tk.labyrinth.jaap.model.declaration.TypeDescription;

import java.util.List;

@Builder
@Value
public class MethodDeclaration {

	List<FormalParameterDeclaration> formalParameters;

	String qualifiedName;

	TypeDescription returnType;
}
