package tk.labyrinth.steward.manifestbuilder.merging;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface Mergeable {

	Class<? extends Merger<?>> merger() default UnsupportedMerger.class;

	MergeStrategy strategy() default MergeStrategy.UNSUPPORTED;
}
