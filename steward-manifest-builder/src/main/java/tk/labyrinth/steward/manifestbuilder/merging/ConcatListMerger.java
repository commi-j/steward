package tk.labyrinth.steward.manifestbuilder.merging;

import tk.labyrinth.misc4j2.collectoin.ListUtils;

import java.util.List;

public class ConcatListMerger extends PickNonnullMergerBase<List<?>> {

	@Override
	@SuppressWarnings("rawtypes")
	protected List<?> mergeNonnull(List<?> oldOne, List<?> newOne) {
		return ListUtils.flatten((List) oldOne, (List) newOne);
	}
}
