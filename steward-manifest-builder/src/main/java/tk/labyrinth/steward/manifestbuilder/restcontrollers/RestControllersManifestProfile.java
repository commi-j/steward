package tk.labyrinth.steward.manifestbuilder.restcontrollers;

import com.google.auto.service.AutoService;
import tk.labyrinth.steward.manifestbuilder.discovery.ElementDiscoverer;
import tk.labyrinth.steward.manifestbuilder.exposure.ElementExposer;
import tk.labyrinth.steward.manifestbuilder.manifest.ManifestProfile;

import java.util.List;

@AutoService(ManifestProfile.class)
public class RestControllersManifestProfile implements ManifestProfile {

	@Override
	public ElementDiscoverer getElementDiscoverer() {
		return new RestControllersElementDiscoverer();
	}

	@Override
	public List<ElementExposer> getElementExposers() {
		return List.of(
				new RequestMappingElementExposer(),
				new RestControllerElementExposer());
	}
}
