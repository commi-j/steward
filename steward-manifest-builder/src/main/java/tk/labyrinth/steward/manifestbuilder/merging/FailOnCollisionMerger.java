package tk.labyrinth.steward.manifestbuilder.merging;

import javax.annotation.Nullable;
import java.util.Objects;

public class FailOnCollisionMerger extends PickNonnullMergerBase<Object> {

	@Override
	protected Object mergeNonnull(Object oldOne, Object newOne) {
		if (!Objects.equals(oldOne, newOne)) {
			throw new IllegalArgumentException("Collision");
		}
		return oldOne;
	}

	@Nullable
	@Override
	public Object merge(@Nullable Object oldOne, @Nullable Object newOne) {
		Object result;
		if (oldOne != null) {
			if (newOne != null) {
				throw new IllegalArgumentException();
			} else {
				result = oldOne;
			}
		} else {
			result = newOne;
		}
		return result;
	}
}
