package tk.labyrinth.steward.manifestbuilder.exposure;

import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.steward.manifestbuilder.model.WritableElement;

import java.util.List;

public interface ElementExposer {

	List<WritableElement> expose(ElementTemplate elementHandle);
}
