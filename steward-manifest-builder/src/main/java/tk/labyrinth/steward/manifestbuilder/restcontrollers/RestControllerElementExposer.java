package tk.labyrinth.steward.manifestbuilder.restcontrollers;

import tk.labyrinth.jaap.annotation.merged.MergedAnnotation;
import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.steward.manifestbuilder.exposure.ElementExposer;
import tk.labyrinth.steward.manifestbuilder.model.WritableElement;

import java.util.List;

public class RestControllerElementExposer implements ElementExposer {

	@Override
	public List<WritableElement> expose(ElementTemplate elementHandle) {
		List<WritableElement> result;
		if (elementHandle.isTypeElement()) {
			MergedAnnotation restControllerAnnotation = elementHandle.getMergedAnnotation(
					RestControllersConstants.REST_CONTROLLER_ANNOTATION_NAME,
					RestControllersConstants.SPRING_MERGED_ANNOTATION_SPECIFICATION);
			if (restControllerAnnotation.isPresent()) {
				result = List.of(WritableElement.asSignatureOf(restControllerAnnotation));
			} else {
				result = List.of();
			}
		} else {
			result = List.of();
		}
		return result;
	}
}
