package tk.labyrinth.steward.manifestbuilder.merging;

import javax.annotation.Nullable;

/**
 * @param <T> Type
 */
public interface Merger<T> {

	@Nullable
	T merge(@Nullable T oldOne, @Nullable T newOne);
}
