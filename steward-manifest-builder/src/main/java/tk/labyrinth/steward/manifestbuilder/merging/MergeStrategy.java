package tk.labyrinth.steward.manifestbuilder.merging;

public enum MergeStrategy {
	CONCAT_LIST(ConcatListMerger.class),
	FAIL_ON_COLLISION(FailOnCollisionMerger.class),
	/**
	 * Default value for {@link Mergeable#strategy()}.
	 */
	UNSUPPORTED(UnsupportedMerger.class);

	private final Class<? extends Merger<?>> mergerType;

	MergeStrategy(Class<? extends Merger<?>> mergerType) {
		this.mergerType = mergerType;
	}
}
