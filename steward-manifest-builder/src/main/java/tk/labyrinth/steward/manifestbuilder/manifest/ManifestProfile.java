package tk.labyrinth.steward.manifestbuilder.manifest;

import tk.labyrinth.steward.manifestbuilder.discovery.ElementDiscoverer;
import tk.labyrinth.steward.manifestbuilder.exposure.ElementExposer;

import java.util.List;

public interface ManifestProfile {

	ElementDiscoverer getElementDiscoverer();

	List<ElementExposer> getElementExposers();
}
