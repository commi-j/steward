package tk.labyrinth.steward.manifestbuilder;

import com.google.auto.service.AutoService;
import lombok.val;
import tk.labyrinth.jaap.context.RoundContext;
import tk.labyrinth.jaap.core.CallbackAnnotationProcessor;
import tk.labyrinth.steward.manifestbuilder.discovery.ElementDiscoverer;
import tk.labyrinth.steward.manifestbuilder.exposure.ElementExposer;
import tk.labyrinth.steward.manifestbuilder.manifest.ManifestProfile;
import tk.labyrinth.steward.manifestbuilder.model.ElementSignature;
import tk.labyrinth.steward.manifestbuilder.model.WritableElement;
import tk.labyrinth.steward.misc4j.java.util.ServiceLoaderUtils;

import javax.annotation.processing.Processor;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@AutoService(Processor.class)
public class ManifestBuilderAnnotationProcessor extends CallbackAnnotationProcessor {

	private Set<ManifestProfile> manifestProfiles;

	{
		onInit(processingEnvironment -> {
			manifestProfiles = ServiceLoaderUtils.load(ManifestProfile.class);
		});
		onEachRound(round -> {
			RoundContext roundContext = RoundContext.of(round);
			//
			manifestProfiles.forEach(manifestProfile -> {
				ElementDiscoverer elementDiscoverer = manifestProfile.getElementDiscoverer();
				List<ElementExposer> elementExposers = manifestProfile.getElementExposers();
				//
				val discoveredElementHandles = roundContext.getAllTypeElements()
						.flatMap(elementDiscoverer::discover)
						.collect(Collectors.toList());
				//
				val exposedEntries = discoveredElementHandles.stream()
						.flatMap(elementHandle -> elementExposers.stream()
								.flatMap(elementExposer -> elementExposer.expose(elementHandle).stream()))
						.collect(Collectors.toList());
				//
				Map<ElementSignature, List<WritableElement>> groupedEntries = exposedEntries.stream()
						.collect(Collectors.groupingBy(WritableElement::getSignature));
				System.out.println();
			});
			System.out.println();
		});
	}
}
