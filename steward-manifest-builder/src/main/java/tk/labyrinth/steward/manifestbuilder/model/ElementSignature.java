package tk.labyrinth.steward.manifestbuilder.model;

import lombok.Value;
import tk.labyrinth.steward.misc4j.exception.NotImplementedException;

@Value
public class ElementSignature {

	String value;

	public ElementSignature getParentSignature() {
		//
		throw new NotImplementedException();
	}

	public static ElementSignature of(String elementSignatureString) {
		return new ElementSignature(elementSignatureString);
	}
}
