package tk.labyrinth.steward.manifestbuilder.restcontrollers;

import tk.labyrinth.jaap.annotation.merged.MergedAnnotation;
import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.steward.manifestbuilder.exposure.ElementExposer;
import tk.labyrinth.steward.manifestbuilder.model.WritableElement;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Exposes Types and Methods.
 */
public class RequestMappingElementExposer implements ElementExposer {

	@Override
	public List<WritableElement> expose(ElementTemplate elementHandle) {
		List<WritableElement> result;
		{
			MergedAnnotation requestMappingAnnotation = elementHandle.getMergedAnnotation(
					RestControllersConstants.REQUEST_MAPPING_ANNOTATION_NAME,
					RestControllersConstants.SPRING_MERGED_ANNOTATION_SPECIFICATION);
			if (requestMappingAnnotation.isPresent()) {
				result = Stream
						.of(
								tryResolveMethod(elementHandle),
								resolveAnnotation(requestMappingAnnotation))
						.filter(Objects::nonNull)
						.collect(Collectors.toList());
			} else {
				result = List.of();
			}
		}
		return result;
	}

	private static WritableElement resolveAnnotation(
			MergedAnnotation requestMappingAnnotation) {
		return WritableElement.asDescriptionOf(requestMappingAnnotation);
	}

	@Nullable
	private static WritableElement tryResolveMethod(
			ElementTemplate elementHandle) {
		WritableElement result;
		if (elementHandle.isMethodElement()) {
			MethodElementTemplate methodElementHandle = elementHandle.asMethodElement();
			result = WritableElement.asDescriptionOf(methodElementHandle, true);
		} else {
			result = null;
		}
		return result;
	}
}
