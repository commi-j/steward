package tk.labyrinth.steward.manifestbuilder.restcontrollers;

import tk.labyrinth.jaap.annotation.merged.MergedAnnotationSpecification;
import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;
import tk.labyrinth.steward.manifestbuilder.discovery.ElementDiscoverer;

import java.util.stream.Stream;

public class RestControllersElementDiscoverer implements ElementDiscoverer {

	private final MergedAnnotationSpecification springMergedAnnotationSpecification = MergedAnnotationSpecification.javaCore()
			.withMetaAnnotation();

	@Override
	public Stream<? extends ElementTemplate> discover(TypeElementTemplate typeElementHandle) {
		Stream<? extends ElementTemplate> result;
		if (typeElementHandle.hasMergedAnnotation(
				RestControllersConstants.REST_CONTROLLER_ANNOTATION_NAME,
				springMergedAnnotationSpecification)) {
			result = Stream
					.concat(
							Stream.of(typeElementHandle),
							//
							// We expect all endpoint methods to be present in@RestController class,
							// so ignoring superclasses.
							typeElementHandle.getDeclaredMethods())
					.filter(elementHandle -> elementHandle.hasMergedAnnotation(
							RestControllersConstants.REQUEST_MAPPING_ANNOTATION_NAME,
							springMergedAnnotationSpecification) ||
							elementHandle.hasMergedAnnotation(
									RestControllersConstants.REST_CONTROLLER_ANNOTATION_NAME,
									springMergedAnnotationSpecification));
		} else {
			result = Stream.empty();
		}
		return result;
	}
}
