package tk.labyrinth.steward.manifestbuilder.exposure;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class AnnotationSignature {

	String value;

	public static AnnotationSignature of(String value) {
		return new AnnotationSignature(value);
	}
}
