package tk.labyrinth.steward.manifestbuilder.model;

import lombok.Value;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotation;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.misc4j2.collectoin.ListUtils;
import tk.labyrinth.steward.manifestmodel.java.FormalParameterDescription;

import java.util.List;
import java.util.stream.Collectors;

@Value
public class WritableElement {

	List<Pair<String, Object>> attributes;

	ElementSignature signature;

	public static WritableElement asDescriptionOf(MergedAnnotation mergedAnnotation) {
		return new WritableElement(
				mergedAnnotation.getAttributes().stream()
						.map(attribute -> Pair.of(attribute.getName(), attribute.getValue()))
						.collect(Collectors.toList()),
				ElementSignature.of(mergedAnnotation.getSignature()));
	}

	public static WritableElement asDescriptionOf(
			MethodElementTemplate methodElementHandle,
			boolean includeFormalParameters) {
		List<Pair<String, Object>> formalParametersAttribute = includeFormalParameters
				? List.of(Pair.of(
				"formalParameters",
				methodElementHandle.getFormalParameters()
						.map(formalParameter -> FormalParameterDescription.builder()
								.name(formalParameter.getSimpleNameAsString())
								.type(formalParameter.getType().getDescription().toString())
								.build())
						.collect(Collectors.toList())))
				: List.of();
		return new WritableElement(
				ListUtils.flatten(formalParametersAttribute),
				ElementSignature.of(methodElementHandle.getSignature()));
	}

	public static WritableElement asSignatureOf(MergedAnnotation mergedAnnotation) {
		return new WritableElement(
				List.of(),
				ElementSignature.of(mergedAnnotation.getSignature()));
	}
}
