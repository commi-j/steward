package tk.labyrinth.steward.manifestbuilder.merging;

import javax.annotation.Nullable;

/**
 * If any of elements is null outcome is to pick other.
 *
 * @param <T> Type
 */
public abstract class PickNonnullMergerBase<T> implements Merger<T> {

	@Nullable
	protected abstract T mergeNonnull(T oldOne, T newOne);

	@Nullable
	@Override
	public T merge(@Nullable T oldOne, @Nullable T newOne) {
		T result;
		if (oldOne != null) {
			if (newOne != null) {
				result = mergeNonnull(oldOne, newOne);
			} else {
				result = oldOne;
			}
		} else {
			result = newOne;
		}
		return result;
	}
}
