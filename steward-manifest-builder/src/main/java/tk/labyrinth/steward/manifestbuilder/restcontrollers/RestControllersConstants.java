package tk.labyrinth.steward.manifestbuilder.restcontrollers;

import tk.labyrinth.jaap.annotation.merged.MergedAnnotationSpecification;

public class RestControllersConstants {

	public static final String REQUEST_MAPPING_ANNOTATION_NAME = "org.springframework.web.bind.annotation.RequestMapping";

	public static final String REST_CONTROLLER_ANNOTATION_NAME = "org.springframework.web.bind.annotation.RestController";

	public static final MergedAnnotationSpecification SPRING_MERGED_ANNOTATION_SPECIFICATION = MergedAnnotationSpecification
			.javaCore()
			.withMetaAnnotation();
}
