package tk.labyrinth.steward.manifestbuilder.model;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class ExposedElementDescription<D> {

	D description;

	String parentSignature;
}
