package tk.labyrinth.steward.manifestbuilder.merging;

import javax.annotation.Nullable;

/**
 * Default value for {@link Mergeable#merger()}.
 */
public class UnsupportedMerger implements Merger<Object> {

	@Override
	public Object merge(@Nullable Object oldOne, @Nullable Object newOne) {
		throw new UnsupportedOperationException();
	}
}
