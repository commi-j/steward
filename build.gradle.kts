@file:Suppress("UNCHECKED_CAST")

plugins {
	`java-library`
	`maven-publish`
	//
	// https://github.com/fkorotkov/gradle-libraries-plugin
	id("com.github.fkorotkov.libraries").version("1.1")
}
//
allprojects {
	group = "tk.labyrinth.steward"
	version = "0.1.0-SNAPSHOT"
	//
	apply {
		`java-library`
		libraries
		`maven-publish`
	}
	//
	repositories {
		mavenCentral()
		mavenLocal()
	}
	//
	dependencies {
		//
		// Maven:compile
		implementation(libraries["com.google.code.findbugs:jsr305"])
		//
		// Maven:provided
		annotationProcessor(libraries["org.projectlombok:lombok"])
		compileOnly(libraries["org.projectlombok:lombok"])
		//
		//
		// Maven:test
		// testImplementation(libraries["org.junit.jupiter:junit-jupiter"])
		testImplementation(libraries["org.springframework.boot:spring-boot-starter-test"])
	}
	//
	tasks {
		val versionJava = "11"
		//
		compileJava {
			sourceCompatibility = versionJava
			targetCompatibility = versionJava
		}
		//
		test {
			useJUnitPlatform()
		}
	}
	//
	extra["configureMavenPublishing"] = {
		val publicationName = "maven"
		//
		publishing {
			publications {
				create<MavenPublication>(publicationName) {
					from(components["java"])
				}
			}
		}
	}
}
//
dependencies {
	//
}
//
//(extra["configureMavenPublishing"] as () -> Unit).invoke()
//
//
// Utility
//
inline val ObjectConfigurationAction.`java-library`: ObjectConfigurationAction
	get() = plugin("java-library")
inline val ObjectConfigurationAction.libraries: ObjectConfigurationAction
	get() = plugin("com.github.fkorotkov.libraries")
inline val ObjectConfigurationAction.`maven-publish`: ObjectConfigurationAction
	get() = plugin("maven-publish")
