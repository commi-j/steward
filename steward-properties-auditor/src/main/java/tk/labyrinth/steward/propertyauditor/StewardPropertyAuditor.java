package tk.labyrinth.steward.propertyauditor;

import tk.labyrinth.jaap.core.CallbackAnnotationProcessor;

import java.util.Set;

public class StewardPropertyAuditor extends CallbackAnnotationProcessor {

	{
		onEachRound(round -> {
			// TODO
		});
	}

	@Override
	public Set<String> getSupportedAnnotationTypes() {
		return Set.of(
				"org.springframework.beans.factory.annotation.Value"
		);
	}
}
