//
dependencies {
	//
	implementation(project(":steward-misc4j"))
	//
	compileOnly("ch.qos.logback:logback-classic:1.2.3")
	//
	compileOnly("io.micrometer:micrometer-core:1.7.0")
	compileOnly("io.micrometer:micrometer-registry-prometheus:1.7.0")
	//
	// FIXME: Replace with smallest required artifact.
	compileOnly("org.springframework.kafka:spring-kafka:2.7.1")
}
//
tasks {
	val versionJava = "15"
	//
	// FIXME: We should declare only version, all other stuff to be hidden in root build file.
	compileJava {
		sourceCompatibility = versionJava
		targetCompatibility = versionJava
	}
}
//
