package tk.labyrinth.steward.metrics.integration;

import io.micrometer.core.instrument.Tag;
import lombok.Getter;

public enum IntegrationDirectionTag {
	INBOUND("in"),
	OUTBOUND("out");

	public static final String NAME = "direction";

	@Getter
	private final String prettyName;

	IntegrationDirectionTag(String prettyName) {
		this.prettyName = prettyName;
	}

	public Tag toMicrometerTag() {
		return Tag.of(NAME, getPrettyName());
	}
}
