package tk.labyrinth.steward.metrics.common;

import io.micrometer.core.instrument.Tag;
import lombok.Getter;

public enum OutcomeTag {
	FAILURE("failure"),
	SUCCESS("success");

	public static final String NAME = "outcome";

	@Getter
	private final String prettyName;

	OutcomeTag(String prettyName) {
		this.prettyName = prettyName;
	}

	public static Tag create(Throwable throwable) {
		return Tag.of(NAME, (throwable == null ? SUCCESS : FAILURE).getPrettyName());
	}
}
