package tk.labyrinth.steward.metrics.logging;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.turbo.TurboFilter;
import ch.qos.logback.core.spi.FilterReply;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.binder.logging.LogbackMetrics;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import tk.labyrinth.steward.metrics.common.CommonTags;
import tk.labyrinth.steward.metrics.common.OutcomeTag;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * See io.micrometer.core.instrument.binder.logging.MetricsTurboFilter#decide(*).
 *
 * @see LogbackMetrics
 */
public class LogbackMetricsReporter extends TurboFilter {

	public static final String LEVEL_TAG_NAME = "level";

	public static final Map<Integer, String> LEVEL_TAG_VALUES = Stream
			.of(
					Level.DEBUG,
					Level.ERROR,
					Level.INFO,
					Level.TRACE,
					Level.WARN)
			.collect(Collectors.toMap(Level::toInt, level -> level.toString().toLowerCase()));

	public static final String METRIC_NAME = "logging_logback_events";

	private static final Object object = new Object();

	private final ThreadLocal<Object> metricReportingInProgress = new ThreadLocal<>();

	@Override
	public FilterReply decide(Marker marker, Logger logger, Level level, String format, Object[] params, Throwable t) {
		//
		// ! COPIED FROM io.micrometer.core.instrument.binder.logging.MetricsTurboFilter.decide(*):
		// When filter is asked for decision for an isDebugEnabled call or similar test, there is no message (ie format)
		// and no intention to log anything with this call. We will not increment counters and can return immediately and
		// avoid the relatively expensive ThreadLocal access below. See also logbacks Logger.callTurboFilters().
		if (format != null) {
			//
			if (metricReportingInProgress.get() == null) {
				metricReportingInProgress.set(object);
				try {
					//
					// ! COPIED FROM io.micrometer.core.instrument.binder.logging.MetricsTurboFilter.decide(*):
					// cannot use logger.isEnabledFor(level), as it would cause a StackOverflowError by calling this filter again!
					if (level.isGreaterOrEqual(logger.getEffectiveLevel())) {
						//
						List<Tag> tags = List.of(
								CommonTags.cause(t),
								CommonTags.exception(t),
								Tag.of(LEVEL_TAG_NAME, LEVEL_TAG_VALUES.get(level.toInt())),
								OutcomeTag.create(t));
						//
						Metrics.counter(METRIC_NAME, tags);
					}
				} finally {
					metricReportingInProgress.remove();
				}
			} else {
				// FIXME: We should use Logback as it is definitely present here.
				LoggerFactory.getLogger(getClass()).trace("Metric was not reported because we are already reporting");
			}
		}
		return FilterReply.NEUTRAL;
	}
}
