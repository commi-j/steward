package tk.labyrinth.steward.metrics.common;

import io.micrometer.core.instrument.Tag;

public class CommonTags {

	public static Tag cause(Throwable throwable) {
		return Tag.of(CommonTagNames.CAUSE, CommonTagValues.cause(throwable));
	}

	public static Tag exception(Throwable throwable) {
		return Tag.of(CommonTagNames.EXCEPTION, CommonTagValues.exception(throwable));
	}
}
