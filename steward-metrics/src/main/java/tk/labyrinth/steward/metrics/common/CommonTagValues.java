package tk.labyrinth.steward.metrics.common;

public class CommonTagValues {

	/**
	 * Prometheus fails to register and record metric with different tag key sets. To avoid this issue you may report
	 * this value where actual tag value is missing.<br>
	 * <br>
	 * See PrometheusMeterRegistry#applyToCollector(Meter.Id,Consumer) for original error.
	 */
	public static final String EMPTY = "_empty";

	public static String cause(Throwable throwable) {
		return throwable != null
				? throwable.getCause() != null
				? throwable.getCause().getClass().getName()
				: EMPTY
				: EMPTY;
	}

	public static String exception(Throwable throwable) {
		return throwable != null
				? throwable.getClass().getName()
				: EMPTY;
	}
}
