package tk.labyrinth.steward.metrics.integration.kafka;

import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.lang.NonNullApi;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerInterceptor;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.metrics.MetricsReporter;
import org.springframework.kafka.listener.ConsumerAwareRecordInterceptor;
import tk.labyrinth.steward.metrics.common.CommonTags;
import tk.labyrinth.steward.metrics.common.OutcomeTag;
import tk.labyrinth.steward.metrics.integration.IntegrationDirectionTag;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @see MetricsReporter
 */
@NonNullApi
public class SpringKafkaMetricsReporter implements
		ConsumerAwareRecordInterceptor<Object, Object>,
		ProducerInterceptor<Object, Object> {

	public static final String METRIC_NAME = "integration_kafka_messages";

	public static final String PARTITION_TAG_NAME = "partition";

	public static final String TOPIC_TAG_NAME = "topic";

	@Override
	public void close() {
		// no-op
	}

	@Override
	public void configure(Map<String, ?> configs) {
		// no-op
	}

	@Override
	public void failure(ConsumerRecord<Object, Object> record, Exception exception, Consumer<Object, Object> consumer) {
		// CONSUMER AFTER (same thread?)
		//
		List<Tag> tags = createAfterTags(exception);
		//
		// TODO: Implement.
	}

	@Override
	public ConsumerRecord<Object, Object> intercept(
			ConsumerRecord<Object, Object> record,
			Consumer<Object, Object> consumer) {
		// CONSUMER BEFORE
		//
		List<Tag> tags = Stream
				.concat(
						createCommonTags(IntegrationDirectionTag.INBOUND, record.topic()).stream(),
						createBeforeTags(record.partition()).stream())
				.collect(Collectors.toList());
		//
		Metrics.counter(METRIC_NAME, tags).increment();
		//
		return record;
	}

	@Override
	public void onAcknowledgement(RecordMetadata metadata, @Nullable Exception exception) {
		// PRODUCER AFTER (different thread)
		//
		List<Tag> tags = createAfterTags(exception);
		//
		// TODO: Implement & find a way to map before & after.
		// TODO: Report max size
	}

	@Override
	public ProducerRecord<Object, Object> onSend(ProducerRecord<Object, Object> record) {
		// PRODUCER BEFORE
		//
		List<Tag> tags = Stream
				.concat(
						createCommonTags(IntegrationDirectionTag.OUTBOUND, record.topic()).stream(),
						createBeforeTags(record.partition()).stream())
				.collect(Collectors.toList());
		//
		Metrics.counter(METRIC_NAME, tags).increment();
		//
		return record;
	}

	@Override
	public void success(ConsumerRecord<Object, Object> record, Consumer<Object, Object> consumer) {
		// CONSUMER AFTER (same thread?)
		//
		List<Tag> tags = createAfterTags(null);
		//
		// TODO: Implement.
	}

	private static List<Tag> createAfterTags(
			@Nullable Throwable throwable) {
		return List.of(
				CommonTags.exception(throwable),
				OutcomeTag.create(throwable));
	}

	private static List<Tag> createBeforeTags(
			@Nullable Integer partition) {
		return List.of(
				createPartitionTag(partition));
	}

	private static List<Tag> createCommonTags(
			IntegrationDirectionTag direction,
			String topic) {
		return List.of(
				direction.toMicrometerTag(),
				createTopicTag(topic));
	}

	private static Tag createPartitionTag(@Nullable Integer partition) {
		return Tag.of(
				PARTITION_TAG_NAME,
				Integer.toString(partition != null ? partition : RecordMetadata.UNKNOWN_PARTITION));
	}

	private static Tag createTopicTag(String topic) {
		return Tag.of(TOPIC_TAG_NAME, topic);
	}
}
