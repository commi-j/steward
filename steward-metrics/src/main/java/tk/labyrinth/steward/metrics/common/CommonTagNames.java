package tk.labyrinth.steward.metrics.common;

public class CommonTagNames {

	public static final String CAUSE = "cause";

	public static final String EXCEPTION = "exception";
}
