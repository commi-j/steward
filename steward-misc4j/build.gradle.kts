@file:Suppress("UNCHECKED_CAST")

dependencies {
    //
    implementation("org.apache.commons:commons-lang3:3.10")
    //
    compileOnly(libraries["com.fasterxml.jackson.core:jackson-databind"])
    compileOnly(libraries["com.fasterxml.jackson.datatype:jackson-datatype-jsr310"])
}
//
(extra["configureMavenPublication"] as () -> Unit).invoke()
