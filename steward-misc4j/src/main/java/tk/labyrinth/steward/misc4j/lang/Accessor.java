package tk.labyrinth.steward.misc4j.lang;

/**
 * @param <T> Type
 *
 * @author Commitman
 * @version 1.0.0
 */
public interface Accessor<T> {

	T get();

	T set(T value);
}
