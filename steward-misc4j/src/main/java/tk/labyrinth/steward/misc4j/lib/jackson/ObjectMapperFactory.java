package tk.labyrinth.steward.misc4j.lib.jackson;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

/**
 * TODO: Create examples of bad behaviour without these settings.<br>
 * <br>
 * Dependencies:<br>
 * - com.fasterxml.jackson.core:jackson-databind;<br>
 * - com.fasterxml.jackson.datatype:jackson-datatype-jsr310;<br>
 *
 * @author Commitman
 * @version 1.0.2
 */
public class ObjectMapperFactory {

	public static ObjectMapper defaultConfigured() {
		ObjectMapper result = new ObjectMapper();
		{
			// Handle fields and creators.
			// Ignore methods.
			result.setDefaultVisibility(JsonAutoDetect.Value.construct(
					JsonAutoDetect.Visibility.ANY,
					JsonAutoDetect.Visibility.NONE,
					JsonAutoDetect.Visibility.NONE,
					JsonAutoDetect.Visibility.NONE,
					JsonAutoDetect.Visibility.ANY
			));
		}
		{
			// Avoiding jsons being overpopulated with null values.
			result.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		}
		{
			// https://github.com/FasterXML/jackson-modules-java8/tree/master/datetime
			// Must be manually enabled in 2.x <= x < 3.x versions.
			// You can try to remove it if you use 3.x version.
			result.registerModule(new JavaTimeModule());
		}
		{
			// https://github.com/FasterXML/jackson-databind/issues/585
			// Ensures Jackson pretty prints with "\n" instead of System.getProperty("line.separator")
			// to keep consistent platform-independent behaviour.
			// The problem was initially found in tests comparing prettified jsons with textblock Strings
			// as latters always use "\n".
			result.setDefaultPrettyPrinter(
					new DefaultPrettyPrinter().withObjectIndenter(
							new DefaultIndenter().withLinefeed("\n")));
		}
		return result;
	}
}
